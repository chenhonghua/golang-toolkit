module gitee.com/chenhonghua/golang-toolkit

go 1.17

require (
	github.com/pkg/sftp v1.13.4
	golang.org/x/crypto v0.0.0-20211215165025-cf75a172585e
)

require (
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/zerolog v1.26.1 // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
	golang.org/x/text v0.3.7 // indirect
)
