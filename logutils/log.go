package logutils

import (
	l "github.com/rs/zerolog/log"
)

func Debug(s string) {
	l.Debug().Msg(s)
}

func Info(s string) {
	l.Info().Msg(s)
}

func Warn(s string) {
	l.Warn().Msg(s)
}

func Error(s string, err error) {
	l.Err(err).Msg(s)
}
