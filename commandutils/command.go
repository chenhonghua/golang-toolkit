package commandutils

import (
	"bytes"
	"errors"
	"os/exec"
)

// linux指令执行器
func Command(command string) (string, error) {
	cmd := exec.Command("/bin/sh", "-c", command)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		return "", errors.New(stderr.String())
	}
	return out.String(), nil
}

// 查看路径，等于ls或whereis
func Look(p string) (string, error) {
	output, err := exec.LookPath(p)
	if err != nil {
		return "", err
	}
	return string(output), nil
}
