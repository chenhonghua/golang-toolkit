package stringutils
// package main

import (
	"fmt"
)

func Byte2Hex(b []byte) string {
	hexstr := fmt.Sprintf("%x", b)
	return hexstr
}

func Substring(s string, beginindex, endindex int) string {
	return string([]rune(s)[beginindex:endindex])
}
