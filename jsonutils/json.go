package jsonutils

import (
	"encoding/json"
	"errors"
)

// 结构体转json
func Struct2Json(v interface{}) (string, error) {
	jsonstrbyte, err := json.Marshal(v)
	if err != nil {
		return "", errors.New("结构体无法序列化:" + err.Error())
	}
	return string(jsonstrbyte), nil
}

// json转结构体
func Json2Struct(j string, v interface{}) error {
	err := json.Unmarshal([]byte(j), v)
	if err != nil {
		return errors.New("json无法反序列化:" + err.Error())
	}
	return nil
}

// json转map
func Json2Map(j string) (map[string]interface{}, error) {
	var m map[string]interface{}
	err := json.Unmarshal([]byte(j), &m)
	if err != nil {
		return nil, errors.New("json无法转化为map:" + err.Error())
	}
	return m, nil
}
