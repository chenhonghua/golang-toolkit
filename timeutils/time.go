package timeutils

import (
	"time"
)

const (
	DateTemplate1 = "2006-01-02"
	DateTemplate2 = "2006/01/02"
	TimeTemplate1 = "23:59:59"
)

func Time2String(t time.Time, timeTemplate string) string {
	return t.Format(timeTemplate)
}

func String2Time(str, timeTemplate string) (time.Time, error) {
	return time.ParseInLocation(timeTemplate, str, time.Local)
}

func Int642String(t int64, timeTemplate string) string {
	return time.Unix(t, 0).Format(timeTemplate)
}
