package main

import (
	"fmt"

	"gitee.com/chenhonghua/golang-toolkit/encodeutils"
)

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("panic", r)
		}
	}()
	// cli, err := sshutils.NewCli("192.168.68.175", "newland", "newland", 22)
	// if err != nil {
	// 	println("error : " + err.Error())
	// }
	// r := cli.Run("free -h")
	// if r.Err != nil {
	// 	fmt.Printf("error : %s , %T \n", r.Err.Error(), r.Err)
	// } else {
	// 	j, _ := json.Marshal(r)
	// 	fmt.Println(string(j))
	// }
	// rs := cli.Upload(sshutils.SftpReq{LocalPath: "./LICENSE", RemotePath: "/root/tmp.220112/license", Overwrite: true})
	// for _, r := range rs {
	// 	if r.Err != nil {
	// 		fmt.Printf("error[%s] : %s , %T \n", r.LocalPath, r.Err.Error(), r.Err)
	// 	} else {
	// 		j, _ := json.Marshal(r)
	// 		fmt.Println(string(j))
	// 	}
	// }
	// rs := cli.Download(sshutils.SftpReq{LocalPath: "./tmp.2201111522", RemotePath: "/root/update.patch.v1.0.3", Overwrite: true})
	// for _, r := range rs {
	// 	if r.Err != nil {
	// 		fmt.Printf("error : %s , %T \n", r.Err.Error(), r.Err)
	// 	} else {
	// 		j, _ := json.Marshal(r)
	// 		fmt.Println(string(j))
	// 	}
	// }
	// fmt.Scanln()
	s := "这是一串不确定字符集的字符串"
	encodeutils.SetSystemEncode(encodeutils.ENCODE_GBK)
	fmt.Println(encodeutils.Encode(s))

}
