package httputils

import (
	"fmt"
	"net/http"

	"github.com/rs/zerolog/log"
)

type Api struct {
	Uri         string
	Handler     func(http.ResponseWriter, *http.Request)
	Description string
}

func RunServer(listenport int, apis []Api) {
	for _, a := range apis {
		http.HandleFunc(a.Uri, a.Handler)
		if len(a.Description) > 0 {
			log.Info().Msg(a.Description)
		}
	}
	http.ListenAndServe("0.0.0.0:"+fmt.Sprint(listenport), nil)
}
