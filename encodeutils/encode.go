package encodeutils

import (
	"errors"
	"strings"

	"golang.org/x/text/encoding/simplifiedchinese"
)

const (
	ENCODE_GBK  string = "GBK"
	ENCODE_UTF8 string = "UTF8"
)

var systemEncode string = ENCODE_UTF8

func SetSystemEncode(encode string) {
	if strings.EqualFold(encode, ENCODE_GBK) || strings.EqualFold(encode, ENCODE_UTF8) {
		systemEncode = encode
	} else {
		panic(errors.New("错误的字符集"))
	}
}

func Encode(s string) string {
	b := []byte(s)
	c := GetStrCoding(b)
	if strings.EqualFold(systemEncode, c) { // 如果系统字符集与当前字符串字符集一致，不用转换
		return s
	} else { // 如果系统字符集与当前字符串字符集不一致
		if strings.EqualFold(systemEncode, ENCODE_UTF8) {
			return Unicode(s)
		} else if strings.EqualFold(systemEncode, ENCODE_GBK) {
			return Gbk(s)
		} else {
			return ""
		}
	}
}

func GetStrCoding(data []byte) string {
	if IsUtf8(data) {
		return ENCODE_UTF8
	} else if IsGBK(data) {
		return ENCODE_GBK
	} else {
		return "UNKNOWN"
	}
}

func IsGBK(data []byte) bool {
	length := len(data)
	var i int = 0
	for i < length {
		if data[i] <= 0x7f {
			//编码0~127,只有一个字节的编码，兼容ASCII码
			i++
			continue
		} else {
			//大于127的使用双字节编码，落在gbk编码范围内的字符
			if data[i] >= 0x81 &&
				data[i] <= 0xfe &&
				data[i+1] >= 0x40 &&
				data[i+1] <= 0xfe &&
				data[i+1] != 0xf7 {
				i += 2
				continue
			} else {
				return false
			}
		}
	}
	return true
}

func preNUm(data byte) int {
	var mask byte = 0x80
	var num int = 0
	//8bit中首个0bit前有多少个1bits
	for i := 0; i < 8; i++ {
		if (data & mask) == mask {
			num++
			mask = mask >> 1
		} else {
			break
		}
	}
	return num
}
func IsUtf8(data []byte) bool {
	i := 0
	for i < len(data) {
		if (data[i] & 0x80) == 0x00 {
			// 0XXX_XXXX
			i++
			continue
		} else if num := preNUm(data[i]); num > 2 {
			// 110X_XXXX 10XX_XXXX
			// 1110_XXXX 10XX_XXXX 10XX_XXXX
			// 1111_0XXX 10XX_XXXX 10XX_XXXX 10XX_XXXX
			// 1111_10XX 10XX_XXXX 10XX_XXXX 10XX_XXXX 10XX_XXXX
			// 1111_110X 10XX_XXXX 10XX_XXXX 10XX_XXXX 10XX_XXXX 10XX_XXXX
			// preNUm() 返回首个字节的8个bits中首个0bit前面1bit的个数，该数量也是该字符所使用的字节数
			i++
			for j := 0; j < num-1; j++ {
				//判断后面的 num - 1 个字节是不是都是10开头
				if (data[i] & 0xc0) != 0x80 {
					return false
				}
				i++
			}
		} else {
			//其他情况说明不是utf-8
			return false
		}
	}
	return true
}

func Gbk(s string) string {
	bs, _ := simplifiedchinese.GBK.NewEncoder().String(s)
	return string(bs)
}

func Unicode(s string) string {
	bs, _ := simplifiedchinese.GBK.NewDecoder().String(s)
	return string(bs)
}
