package ioutils

import (
	"errors"
	"io/fs"
	"io/ioutil"
	"os"
	"strings"
)

// 文件复制
func CopyFile(src, dest string) error {
	srcByte, err := ioutil.ReadFile(src)
	if err != nil {
		return errors.New("无法读取文件" + src)
	}
	err = ioutil.WriteFile(dest, srcByte, os.ModePerm)
	if err != nil {
		return errors.New("无法写入文件" + dest)
	}
	return nil
}

// 获取父级目录
func ParentDirectory(p string) string {
	return string([]rune(p)[0:strings.LastIndex(p, "/")])
}

// 路径不存在
func PathIsExist(p string) bool {
	_, err := os.Stat(p)
	return err == nil
}

// 创建目录
func CreateDir(p string) error {
	return os.Mkdir(p, os.ModePerm)
}

// 递归创建目录
// 不会产生异常
func CreateDirRecursive(p string) {
	if !PathIsExist(ParentDirectory(p)) { // 父级路径不存在
		CreateDirRecursive(ParentDirectory(p))
	}
	if !PathIsExist(p) {
		err := CreateDir(p)
		if err != nil {
			println("递归创建目录[" + p + "]异常：" + err.Error())
		}
	}
}

// 删除目录
func DeleteDir(p string) error {
	return os.RemoveAll(p)
}

// 列出目录
func ListDir(p string) ([]fs.FileInfo, error) {
	return ioutil.ReadDir(p)
}
